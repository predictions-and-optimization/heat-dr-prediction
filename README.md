# Heat DR Prediction

Module that implements the thermal energy production prediction models based on neural networks and CFD simulations.